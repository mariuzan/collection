import Characters from "../Characters/index";
import "./style.css";
import { useState } from "react";
const CharacterGrid2 = ({ list }) => {
  const [search, setSearch] = useState("");
  const getImages = (url) => {
    const brokenUrl = url.split("/");
    const id = brokenUrl[brokenUrl.length - 2];
    return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`;
  };
  const searchField = (e) => {
    setSearch(e.target.value.toLowerCase());
  };
  const [tasks, setTasks] = useState([]);

  function addNewTask(task) {
    const itensCopy = Array.from(tasks);
    itensCopy.push({ id: tasks.length, value: task });
    setTasks(itensCopy);
  }

  function updateTask({ target }, index) {
    const itensCopy = Array.from(tasks);
    itensCopy.splice(index, 1, { id: index, value: target.value });
    setTasks(itensCopy);
  }

  function deleteTask(index) {
    const itensCopy = Array.from(tasks);
    itensCopy.splice(index, 1);
    setTasks(itensCopy);
  }
  return (
    <>
      <input
        className="searchField"
        onChange={searchField}
        type="text"
        placeholder="Buscar"
        autoFocus
      />
      <div className="gridcards">
        {search
          ? list
              .filter((text) => text.name?.toLowerCase().includes(search))
              .map(({ name, url }, index) => (
                <Characters
                  key={index}
                  name={name}
                  image={getImages(url)}
                  onSubmit={addNewTask}
                />
              ))
          : list.map(({ name, url }, index) => (
              <Characters
                key={index}
                name={name}
                image={getImages(url)}
                onSubmit={addNewTask}
              />
            ))}
      </div>
    </>
  );
};

export default CharacterGrid2;

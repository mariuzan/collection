import Characters from "../Characters/index";
import "./style.css";
import { useState } from "react";
import { DataGrid } from "@material-ui/data-grid";

const CharacterGrid = ({ list, favorites, setFavorites }) => {
  const [search, setSearch] = useState("");
  const [page, setPage] = useState(1);
  const searchField = (e) => {
    setSearch(e.target.value.toLowerCase());
  };
  return (
    <>
      <input
        className="searchField"
        onChange={searchField}
        type="text"
        placeholder="Buscar"
        autoFocus
      />
      <div className="gridcards">
        {search
          ? list
              .filter((text) => text.name?.toLowerCase().includes(search))
              .map(({ name, image }, index) => (
                <Characters
                  key={index}
                  name={name}
                  image={image}
                  favorites={favorites}
                  setFavorites={setFavorites}
                />
              ))
          : list.map(({ name, image }, index) => (
              <Characters
                key={index}
                name={name}
                image={image}
                favorites={favorites}
                setFavorites={setFavorites}
              />
            ))}
      </div>
    </>
  );
};

export default CharacterGrid;

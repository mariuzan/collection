import { makeStyles } from "@material-ui/core/styles";
import { useState, useEffect } from "react";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import "../Characters/style.css";
const Characters = ({ name, image, favorites, setFavorites }) => {
  const useStyles = makeStyles({
    root: {
      margin: 10,
      minWidth: 275,
    },
    bullet: {
      display: "inline-block",
      margin: "0 2px",
      transform: "scale(0.8)",
    },
    title: {
      fontSize: 14,
    },
    pos: {
      marginBottom: 12,
    },
  });

  const handleAddFavorite = (char) => {
    /*const alreadyExist = favorites.find((element) => element.id === char.id);
    if (alreadyExist) {
      return;
    }*/
    setFavorites([...favorites, char]);
  };
  useEffect(() => {
    localStorage.setItem("favoriteList", JSON.stringify(favorites));
  }, [favorites]);
  const classes = useStyles();
  return (
    <>
      <Card className={classes.root}>
        <CardContent>
          <img src={image}></img>
          <Typography variant="body2" component="p">
            {name} <br />
          </Typography>
        </CardContent>
        <CardActions>
          <Button onClick={() => handleAddFavorite(name)} size="small">
            Adicionar aos Favoritos
          </Button>
        </CardActions>
      </Card>
    </>
  );
};

export default Characters;

const FavoritesList = ({ onChange, onDelete, value }) => {
  return (
    <div>
      <input value={value} onChange={onChange} />
      <button onClick={onDelete}>Excluir</button>
    </div>
  );
};

import CharactersGridPokemon from "../../components/CharactersGrid/index2";
const Favoritos = ({ list }) => {
  return (
    <>
      {list ? (
        <CharactersGridPokemon list={list} />
      ) : (
        <div>
          <h1>Favoritos</h1>
          <span>Sem itens por enquanto</span>
        </div>
      )}
    </>
  );
};
export default Favoritos;

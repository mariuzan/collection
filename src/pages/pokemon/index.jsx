import { useState, useEffect } from "react";
import axios from "axios";
import CharacterGrid from "../../components/CharactersGrid/index2";
import Button from "@material-ui/core/Button";

const Pokemon = () => {
  const url = "https://pokeapi.co/api/v2/pokemon?limit=150";
  const [next, setNext] = useState(0);
  const [characterList, setCharacter] = useState([]);

  //metodo GET para preenchimento da lista
  const getCharacters = () => {
    axios
      .get(url)
      .then((response) => {
        setCharacter([...characterList, ...response.data.results]);
      })
      .catch(() => {
        console.log("erro");
      });
  };

  //lifecycle do componente
  useEffect(() => {
    getCharacters();
  }, [next]);

  return (
    <>
      <h1>Pokemon</h1>
      <CharacterGrid list={characterList}></CharacterGrid>
    </>
  );
};
export default Pokemon;

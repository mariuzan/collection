import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Link from "@material-ui/core/Link";
import "./style.css";
const Header = () => {
  return (
    <div className="header">
      <Breadcrumbs aria-label="breadcrumb">
        <Link color="primary" href="rickandmorty">
          Rick and morty
        </Link>
        <Link color="primary" href="/pokemon">
          Pokemon
        </Link>
        <Link color="primary" href="/favoritos">
          Favoritos
        </Link>
      </Breadcrumbs>
    </div>
  );
};

export default Header;

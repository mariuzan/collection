import { Switch, Route } from "react-router-dom";
import RickAndMorty from "../rickandmorty";
import Pokemon from "../pokemon";
import Favoritos from "../favoritos/index";
import Header from "./Header";
const Home = () => {
  return (
    <>
      <Header />
      <Switch>
        <Route exact path="/"></Route>
        <Route path="/rickandmorty">
          <RickAndMorty />
        </Route>
        <Route path="/pokemon">
          <Pokemon />
        </Route>
        <Route path="/favoritos">
          <Favoritos />
        </Route>
      </Switch>
    </>
  );
};

export default Home;

import { useState, useEffect } from "react";
import axios from "axios";
import CharacterGrid from "../../components/CharactersGrid/index";
import Button from "@material-ui/core/Button";

const RickAndMorty = () => {
  //states
  const [characterList, setCharacter] = useState([]);
  const [nextUrl, setUrl] = useState([
    "https://rickandmortyapi.com/api/character?page=1",
  ]);
  const [favorites, setFavorites] = useState(() => {
    const favoriteList = localStorage.getItem("favoriteList");
    if (favoriteList) {
      return JSON.parse(favoriteList);
    }
    return [];
  });
  //metodo GET para preenchimento da lista
  const getCharacters = () => {
    axios
      .get(nextUrl)
      .then((response) => {
        window.localStorage.setItem("rickAndMorty", response.data.results);
        setCharacter([...characterList, ...response.data.results]);
        setUrl(response.data.info.next);
      })
      .catch(() => {
        console.log("erro");
      });
  };
  //lifecycle do componente

  useEffect(() => {
    if (!nextUrl) {
      return;
    }
    getCharacters();
  }, [nextUrl]);
  return (
    <>
      <h1>Rick and morty</h1>

      <CharacterGrid
        list={characterList}
        favorites={favorites}
        setFavorites={setFavorites}
      ></CharacterGrid>
    </>
  );
};
export default RickAndMorty;
